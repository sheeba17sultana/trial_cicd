package com.academy.DAO;

import java.util.List;

import com.model.pojo.Classes;
import com.model.pojo.Student;
import com.model.pojo.Subject;
import com.model.pojo.Teacher;

public interface ClassDAO {
	public Classes createClasses(Classes classes);
	public Classes getClassById(int id);
	public List<Classes> getAllClasses();
	public void removeClass(int id);
	public Classes updateClass(Classes classes);
	public List<Subject> updateSubject(Classes classes);
	//public List<Student> getAllStudents();
	public List<Student> updateStudent(Classes classes);
	public Teacher getTeacherId(int id);
	public List<Teacher> getAllTeacher(int id);
	public Teacher updateTeacher(Teacher teacher);
	
	public Student getStudentId(int id);
	//public List<Subject> getAllSubjects();
	public List<Student> getAllStudents(int id);
	public List<Subject> getAllSubjects();
}

package com.academy.DAO.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import com.academy.DAO.StudentDAO;
import com.model.pojo.Classes;
import com.model.pojo.Student;

public class StudentDAOImpl implements StudentDAO{

	Configuration configuration=new Configuration().configure();
	StandardServiceRegistryBuilder builder=new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
	SessionFactory factory=configuration.buildSessionFactory(builder.build());
	@Override
	public Student createStudent(Student student) {
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(student);
		transaction.commit();
		session.close();
		return student;
	}
	@Override
	public Student getStudentById(int id) {
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		Student student = (Student) session.get(Student.class, id);
		session.save(student);
		transaction.commit();
		session.close();
		return student;
	}
	@Override
	public List<Student> getAllStudentList() {
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		List<Student> studentList = session.createQuery("from com.model.pojo.Student").list();
		transaction.commit();
		session.close();
		return studentList;
	}
	@Override
	public void removeStudent(int id) {
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		Student s = new Student();
		s.setStudentId(id);
		session.delete(s);
		transaction.commit();
		session.close();
	}
	@Override
	public Student updateStudent(Student student) {
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		session.update(student);
		transaction.commit();
		session.close();
		return student;
	}


}

package com.academy.controller;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;
@Provider
public class CorseResponseFilter implements ContainerResponseFilter {

	
	public void filter(ContainerRequestContext requestcontext,
			ContainerResponseContext responsecontext) throws IOException {
		MultivaluedMap<String, Object>headers =responsecontext.getHeaders();
		headers.add("Access-Control-Allow-Origin", "*");
		headers.add("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,PATCH");
		headers.add("Access-Control-Allow-Headers", "X-Requested-with,Content-Type");
		
		// TODO Auto-generated method stub
		
	}

}
